function fillModal(productId, image, language, productname, price, availability, isbnno, publisher, noofpages, publicationyear, binding, imprint, edition, condition, subject, productdescription) {
  sessionStorage.ProductID = productId;
  $('#productimagmodal').attr('src', decodeURI(productimgpath + image));
  $('#languagemodal').html(decodeURI(language));
  $('#productnamemodal').html(decodeURI(productname));
  $('#productpricemodal').html('₹ ' + decodeURI(price));
  $('#productavailibiltiymodal').html(decodeURI(availability) == 'null' ? 'In a Stock' : decodeURI(availability));
  $('#isbnnomodal').html(decodeURI(isbnno));
  $('#publishermodal').html(decodeURI(publisher));
  $('#noofpages').html(decodeURI(noofpages));
  $('#publicationyearmodal').html(decodeURI(publicationyear));
  $('#bindingmodal').html(decodeURI(binding));
  $('#imprintmodal').html(decodeURI(imprint));
  $('#editionmodal').html(decodeURI(edition));
  $('#conditionmodal').html(decodeURI(condition));
  $('#subjectmodal').html(decodeURI(subject));
  $('#productdescriptionmodal').html(decodeURI(productdescription));

}
function insUpdCard(event) {

  var MasterData = {

    "UniqueID": '0',
    "UserID": Cookies.get('permUserId') ? Cookies.get('permUserId') : Cookies.get('tempUser'),
    "Product_ID": sessionStorage.ProductID,
    "Quantity": $('#productquantity').val(),
    "EntryFrom": '',
    "Price": $('#productpricemodal').text().replace('₹ ', ''),
    "DiscountedPrice": '',

  };
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "usercart";
  ajaxpost(path, 'addToCart', 'comment', MasterData, 'control')

}
function addToCart(data) {
  data = JSON.parse(data)
  var data1 = data.recordset[0]

  if (data1.ReturnValue == "1" || data1.ReturnValue == "0") {
    window.location = '/User/Cart';
  }
}

function generateOTP() {

  // Declare a digits variable  
  // which stores all digits 
  var digits = '0123456789';
  let OTP = '';
  for (let i = 0; i < 4; i++) {
    OTP += digits[Math.floor(Math.random() * 10)];
  }
  return OTP;


}




function fetchShoppingCount() {
  var userId = Cookies.get('permUserId') ? Cookies.get('permUserId') : Cookies.get('tempUser');
  var path = serverpath + "usercart/" + userId + "";
  ajaxget(path, 'shoppingcount', 'comment', 'control');
};
function shoppingcount(data, control) {
  data = JSON.parse(data);
  data1 = data.recordset;

  if (data.recordset.length) {

    $('#cartqunatity').html(data1.length);
  }
}
function sendmsg(Msg, Mobile) {


  var parameter = {
    "sender": "PUSTOK",
    "route": "4",
    "country": "91",
    "sms": [
      {
        "message": Msg,
        "to": [Mobile]

      },

    ]
  }
  parameter = JSON.stringify(parameter);
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": serverpath + 'sendsms?' + parameter + '',
    "method": "GET",
    "headers": {
      "content-type": "application",
      "Access-Control-Allow-Origin": "*"
    },
  }
  $.ajax(settings).done(function (response) {
    console.log(response);
  });
}
// pustakeinapp@gmail.com


function profile(){
  var mainmenu='' 
if(Cookies.get('permUserId') ) {
   mainmenu +=` <ul class="main-menu menu-right li-last-0">
               <li class="menu-item has-children">
                   <a href="javascript:void(0)"><p style="font-size: 13px;margin-bottom: 0;"><strong> Profile</strong>  </p></a>
                   <ul class="sub-menu">
                       <li style="font-weight: 700;" id="loginusername"> `+Cookies.get('username')+` </li>
                       <li> <a href="/User/Dashboard">Dashboard</a></li> 
                       <li> <a href="/User/Cart">Cart</a></li>
                       <li> <a href="/" onclick="Cookies.remove('permUserId');Cookies.remove('username')">Logout</a></li>
                   </ul>
               </li></ul>`
}
$('#Profile').append(mainmenu);

}
function searchTextInTable(inputid,tblName) {
  var tbl;
  tbl = tblName;
  jQuery("#" + tbl + " tr:has(td)").hide(); // Hide all the rows.

  var sSearchTerm = jQuery('#'+inputid).val(); //Get the search box value

  if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
  {
      jQuery("#" + tbl + " tr:has(td)").show();
      return false;
  }
  //Iterate through all the td.
  jQuery("#" + tbl + " tr:has(td) td").each(function () {
   
      var cellText = jQuery(this).text().toLowerCase();
      if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
      {
          jQuery(this).parent().show();
          return true;
      }
   
  });
  //e.preventDefault();
}


