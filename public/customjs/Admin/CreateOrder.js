function addNewProductRow() {
    var x = $('.addproductrow').length;
    var productrow = `<div class="row addproductrow" id="productrow`+ x + `" style="width:100%">
    <div class="col-md-6 col-12 mb--20">
    
        <label>Board</label> 
        <select class='boarddl' id="board`+ x +`" onchange='FillBoardwiseClass(this.value,`+ x +`)' style="width: 100%;">
        </select>
    </div>
    <div class="col-md-6 col-12 mb--20">
    
        <label>Class</label>
        <select  class="ddlclass" id="class`+ x +`" onchange='FillBoardClassProduct(`+ x +`)' style="width: 100%;">
        </select>
    </div>
    <div class="col-md-6 col-12 mb--20">
    
        <label>Books</label>
        <select  id="product`+ x +`" style="width: 100%;">
        </select>
    </div>
    <div class="col-md-3 col-12 mb--20">
    
        <label>Quantity</label>
        <input type='number' id="quantity`+ x +`" style="width: 100%;" onfocusout="setAmount()" value=''/>
        
    </div>
    <div class="col-md-3 col-12 mb--20">
    
      
        <button class="btn btn--primary w-100" style=" margin-top: 18%;background-color: red;" onclick="$('#productrow`+ x+`').remove();setAmount();" >Remove </button>
												

    </div>`;
    $('.mainrow').after(productrow);
    FillBoard(x);
    FillBoardwiseClass('6',x);
    FillBoardClassProduct(x);
    
    jQuery("#product"+x+"").append(jQuery("<option ></option>").val("0").html("Select Books"));
}

function FillBoard(x){
    sessionStorage.changeDdlId=x;
    var path = serverpath + "boardmaster/0/1"
    ajaxget(path,'parsedataboard','comment','control');
  
}
function parsedataboard(data){
    data = JSON.parse(data)
    var x=sessionStorage.changeDdlId;
    var data1=data.recordset;
        jQuery("#board"+x+"").empty();
        jQuery("#board"+x+"").append(jQuery("<option ></option>").val("0").html("Select Board"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#board"+x+"").append(jQuery("<option></option>").val(data1[i].BoardID).html(data1[i].BoardName));
        }
    $('#board'+x+'').addClass('nice-select');
}


function FillBoardwiseClass(boardid,x){
    sessionStorage.changeDdlId=x;
    var path = serverpath + "BoardClass/"+boardid+"/1"
    ajaxget(path,'parsedataboardwiseclass','comment','control');
  
}
function parsedataboardwiseclass(data,control){
    data = JSON.parse(data)
 var x=sessionStorage.changeDdlId;
    var data1=data.recordset;
        jQuery("#class"+x+"").empty();
        jQuery("#class"+x+"").append(jQuery("<option ></option>").val("0").html("Select Class"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#class"+x+"").append(jQuery("<option></option>").val(data1[i].ClassID).html(data1[i].ClassName));
        }
        $("#class"+x+"").addClass('nice-select');
}
function FillBoardClassProduct(x) {
    sessionStorage.changeDdlId=x;
    
    $("#product"+x+"").addClass('nice-select');
    var path = serverpath + "boardclasswiseproduct/"+$('#board'+ x +'' ).val()+"/"+$('#class'+ x +'' ).val()+"/1";
   ajaxget(path,'fillProductDdl','comment','control');
}
function fillProductDdl(data,control){
    data = JSON.parse(data)
 var x=sessionStorage.changeDdlId;
    var data1=data.recordset;
    
    $("#product"+x+"").removeClass('nice-select');
        jQuery("#product"+x+"").empty();
        jQuery("#product"+x+"").append(jQuery("<option ></option>").val("0").html("Select Books"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#product"+x+"").append(jQuery("<option></option>").val(data1[i].ProductID+' '+data1[i].Price).html(data1[i].ProductName));
        }
        $("#product"+x+"").addClass('nice-select');
}
function setAmount(){
    var subtotal=0;
    $('.addproductrow').each(function (i) {
      
       var quantity=parseInt($('#quantity'+i+'').val());
       var strProduct=$('#product'+i+'').val().split(' ');
       var price=parseInt(strProduct[1]);
       var multiple=quantity * price;
       subtotal +=multiple;
     
    });
    $('#subtotal').val(subtotal +'₹');
    var shipping=0;
    var grandtotal=0;
    if(subtotal>2000){
        $('#shippingcharge').val('0 ₹');
    }
    else{
        shipping=200;
        $('#shippingcharge').val('200 ₹');
    }
    grandtotal=subtotal+shipping;

$('#total').val(grandtotal+ '₹');

}
function checkValidiation(){
    if($.trim($('#firstname').val())==''){
        toastr.warning('Please Enter First Name');
        return false;
    }
if($.trim($('#lastname').val())==''){
    toastr.warning('Please Enter Last Name');
    return false;
}
if($.trim($('#mobilenumberuser').val())==''){
    toastr.warning('Please Enter Mobile Number');
    return false;
}
if($('#mobilenumberuser').val().length<10 && $.trim($('#mobilenumberuser').val())==''){
    toastr.warning('Please Enter Correct Mobile Number');
    return false;
}
if($.trim($('#useraddress').val())==''){
    toastr.warning('Please Enter Address');
    return false;
}
else{
    insupdUserDetail();
}
}
function insupdUserDetail(){
    var MasterData = {
    
        "UserDetailID":'0',
        "FirstName":$('#firstname').val(),
        "LastName":$('#lastname').val(),
        "EmailAddress":$('#emailaddress').val(),
        "Address":$('#useraddress').val(),
        "StreetAddress2":'0',
        "City":'0',
        "State":'0',
        "PostalCode":"0",
        "Country":"0",
        "Telephone":$('#mobilenumberuser').val(),
        "TempUserID":10
       
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "user";
ajaxpost(path, 'userDetail', 'comment', MasterData, 'control')

}
function userDetail(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
      if (data1.ReturnValue == "1" || data1.ReturnValue=='2' ) {
        insUpdOrder(data1.UserDetailID);
       }
 }
 function insUpdOrder(userId){
    var MasterData = {
        
        "OrderID":'0',
        "UserID":userId,
        "Subtotal":$('#subtotal').val().replace('₹',''),
        "ShippingCharge":$('#shippingcharge').val().replace('₹',''),
        "TotalAmount":$('#total').val().replace('₹',''),
        "PaymentMode":'Cash on Delivery',
        "Address":'Bhopal',
        "OrderDetails":[],
        "Status":'Pending',
        "ReferenceID": Cookies.get('referralID') ? Cookies.get('referralID') : 0
};

$('.addproductrow').each(function (i) {
      
    var quantity=$('#quantity'+i+'').val();
    var strProduct=$('#product'+i+'').val().split(' ');
    var orderdetail= {
        "PRODUCT_ID":strProduct[0],
        "QUANTITY":quantity
    }
  MasterData.OrderDetails.push(orderdetail);
});
 
MasterData = JSON.stringify(MasterData);
var path = serverpath + "Orders";
ajaxpost(path, 'ordersCreate', 'comment', MasterData, 'control');
}
function ordersCreate(data){
    data = JSON.parse(data);
    var data1=data.recordset[0];
    if (data1.ReturnValue == "1") {
        toastr.success('Order Created Successfully');
        resetMode();
        return true;
       }
}
function resetMode(){
    $('#firstname').val(''),
    $('#lastname').val(''),
    $('#emailaddress').val(''),
    $('#useraddress').val(''),
    $('#mobilenumberuser').val(''),
    $('#subtotal').val(''),
    $('#shippingcharge').val(''),
    $('#total').val('')
    $('.addproductrow').each(function (i) {
        if(i>0){
            $('#productrow'+ i+'').remove();
        }
        $('#class0').val('');
        $('#board0').val('');
        $('#quantity0').val('');
        $('#product0').val('');
    });
}