$('#upload').submit(function() {
    $(this).ajaxSubmit({
        error: function(xhr) {
            toastr.warning(xhr.status, "", "info");
        },
        success: function(response) {
            if (response == "Error No File Selected"){
                toastr.warning(response, "", "info");
            }
            else if (response == "File Size Limit Exceeded"){
                toastr.warning(response, "", "info");
            }
            else if (response == "Error: PDF and Image File Only!"){
                toastr.warning(response, "", "info");
            }
            else{
                var str = response;
                
                var res = str.split("!");
                var res1=res[1].split(";");
                sessionStorage.setItem("Productimage1", res1[0])
                sessionStorage.setItem("Productimage2", res1[1])
                sessionStorage.setItem("Productimage3", res1[2])
               InsUpdProductDetail();
            }
        }
    });
    return false;
 });

function InsUpdProductDetail() {

    var MasterData = {
        
            "ProductID":'0',
            "ApplicationID":"1",
            "BoardID":$('#boarddl').val(),
            "ClassID":$('#classddl').val(),
            "ProductName":$('#productname').val(),
            "Availability":$('#availaibility').val(),
            "AuthorName":$('#authorname').val(),
            "ISBNNO":$('#isbnno').val(),
            "Publisher":$('#publisher').val(),
            "NoOfPages":$('#noofpages').val(),
            "PublicationYear":$('#publicationyear').val(),
            "Binding":$('#binding').val(),
            "Imprint":$('#imprint').val(),
            "Edition":$('#edition').val(),
            "Condition":$('#condition').val(),
            "Subject":$('#subject').val(),
            "Language":$('#language').val(),
            "Image1":sessionStorage.Productimage1,
            "Image2":sessionStorage.Productimage2,
            "Image3":sessionStorage.Productimage3,
            "ProductDescription":$('#productdescription').val(),
            "Active_YN":$('#activestatus')[0].checked?1:0,
            "EnterBy":"0",
            "EnterFrom":"0",
            "Price":$('#price').val()
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "productdetail";
    ajaxpost(path, 'parsrdataproductdetail', 'comment', MasterData, 'control')
}

function parsrdataproductdetail(data) {
      data = JSON.parse(data)
   var data1=data.recordset[0]

       if (data1.ReturnValue == "1") {
        resetmode();
        FetchMaxproductId();
        toastr.success("Submit Successful", "", "success")
        
        return true;
    }
     else if (data[0][0].ReturnValue == "2") {
     
        return true;
    }
   

}
function resetmode(){
          $('#boarddl').val('0'),
          $('#classddl').val('0'),
          $('#productname').val(''),
          $('#availaibility').val(''),
          $('#authorname').val(''),
          $('#isbnno').val(''),
           $('#publisher').val(''),
           $('#noofpages').val(''),
          $('#publicationyear').val(''),
          $('#binding').val(''),
           $('#imprint').val(''),
           $('#edition').val(''),
           $('#condition').val(''),
           $('#subject').val(''),
           $('#language').val(''),
           sessionStorage.Productimage='',
           $('#productdescription').val(''),
           $('#activestatus')[0].checked=false
}



   
function FetchMaxproductId() {
    var path = serverpath + "getmaxproductid"
   ajaxget(path,'parsemaxproductid','comment','control');
}
function parsemaxproductid(data,control){  
    data = JSON.parse(data)
 
   // console.log(data.recordset[0].ProductID)
    Cookies.set('ProductID', data.recordset[0].ProductID, { expires: 1, path: '/' });
}
function FillBoard(control){
    var path = serverpath + "boardmaster/0/1"
    ajaxget(path,'parsedataboard','comment',control);
  
}
function parsedataboard(data,control){
    data = JSON.parse(data)
 
    var data1=data.recordset;
        jQuery("#boarddl").empty();
        jQuery("#boarddl").append(jQuery("<option ></option>").val("0").html("Select Board"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#boarddl").append(jQuery("<option></option>").val(data1[i].BoardID).html(data1[i].BoardName));
        }
    $('#boarddl').addClass('nice-select');
}


function FillBoardwiseClass(boardid){
    var path = serverpath + "BoardClass/"+boardid+"/1"
    ajaxget(path,'parsedataboardwiseclass','comment','control');
  
}
function parsedataboardwiseclass(data,control){
    data = JSON.parse(data)
 
    var data1=data.recordset;
        jQuery("#classddl").empty();
        jQuery("#classddl").append(jQuery("<option ></option>").val("0").html("Select Class"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#classddl").append(jQuery("<option></option>").val(data1[i].ClassID).html(data1[i].ClassName));
        }
        $("#classddl").addClass('nice-select');
}
function FillSubject(){
    var path = serverpath + "subjectmaster/0/1"
    ajaxget(path,'parsedatasubject','comment','control');
  
}
function parsedatasubject(data,control){
    data = JSON.parse(data)
 
    var data1=data.recordset;
        jQuery("#subject").empty();
        jQuery("#subject").append(jQuery("<option ></option>").val("0").html("Select Subject"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#subject").append(jQuery("<option></option>").val(data1[i].SubjectID).html(data1[i].SubjectName));
        }
        $("#subject").addClass('nice-select');
}