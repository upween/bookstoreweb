function fetchCheckoutBill() {
    var userId=Cookies.get('permUserId')?Cookies.get('permUserId'):Cookies.get('tempUser');
    var path = serverpath + "usercart/"+userId+"";
    ajaxget(path,'CheckoutBill','comment','control');
};
function CheckoutBill(data,control){ 
    sessionStorage.OrderDetails=data;
    data=JSON.parse(data);
    data1=data.recordset;
    var appenddata='';
    var subtotal=0;

    $('#productlistcheckout').empty();
    if(data.recordset.length){

        for(var i=0 ; i < data1.length ; i++){
            var total=0;
            
            total=parseInt(data1[i].Price.replace('?',''))*parseInt(data1[i].Quantity);
            appenddata += 
            `<li><span title='`+data1[i].ProductName+`' class="left" style='width: 80%;text-overflow: ellipsis; white-space: nowrap; overflow: hidden;'>`+data1[i].ProductName+`   </span> X    <strong>`+data1[i].Quantity+`</strong> <span
            class="right">₹ `+data1[i].Price+`</span></li>`;
            subtotal +=total;
        }
        $('#productlistcheckout').append(appenddata);
        $('#subtotaltocheckoutbill').html('₹'+subtotal);
        var shipping=0;
        var grandtotal=0;
        if(subtotal>2000){
            $('#shippingcosttocheckoutbill').html('₹ 0');
        }
        else{
            shipping=200;
            $('#shippingcosttocheckoutbill').html('₹ 200');
        }
        grandtotal=subtotal+shipping;

        $('#grandtotaltocheckoutbill').html('₹'+grandtotal);
    }
};


function checkValidiation(){
    if(!Cookies.get('permUserId')){
        $('#loginModal').modal('toggle');
        $('#mobilenumber').val($('#mobilenumberuser').val());
        sessionStorage.loginfrom='checkout';
    }
    else{
        if($.trim($('#firstname').val())==''){
           //  toastr.warning('Please Enter First Name');
            getvalidated('firstname','text','First Name');
            return false;
        }
        if($.trim($('#lastname').val())==''){
            // toastr.warning('Please Enter Last Name');
            getvalidated('lastname','text','Last Name');
            return false;   
        }
        if($.trim($('#emailaddress').val())==''){
           //  toastr.warning('Please Enter Email Address');
             getvalidated('emailaddress','email','Email Address');
            return false;
        }

        if($.trim($('#mobilenumberuser').val())==''){
            // toastr.warning('Please Enter Mobile Number');
            getvalidated('mobilenumberuser','number','Mobile Number');
            return false;
            
        }
        if($('#mobilenumberuser').val().length<10 && $.trim($('#mobilenumberuser').val())==''){
           //  toastr.warning('Please Enter Correct Mobile Number');
            getvalidated('mobilenumberuser','number','Mobile Number');
            return false;
        }
        if($.trim($('#useraddress').val())==''){
           //  toastr.warning('Please Enter Address');
            getvalidated('useraddress','text','Address');
            return false;
        }
        if($.trim($('#postcode').val())==''){
           //  toastr.warning('Please Enter Postal Code');
             getvalidated('postcode','number','Postal Code');
            return false;
        }
        else{
            if(!Cookies.get('permUserId')){
                $('#loginModal').modal('toggle');
                $('#mobilenumber').val($('#mobilenumberuser').val());
                sessionStorage.loginfrom='checkout';
            }
            else{
                insupdUserDetail();
            }
        }  
    }
}
function insupdUserDetail(){
  
    var MasterData = {
        "UserDetailID":'0',
        "FirstName":$('#firstname').val(),
        "LastName":$('#lastname').val(),
        "EmailAddress":$('#emailaddress').val(),
        "Address":$('#useraddress').val(),
        "StreetAddress2":'0',
        "City":'1',
        "State":'0',
        "PostalCode":$('#postcode').val(),
        "Country":"0",
        "Telephone":$('#mobilenumberuser').val(),
        "TempUserID":Cookies.get('tempUser')
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "user";
    ajaxpost(path, 'userDetail', 'comment', MasterData, 'control')
}
function userDetail(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
    if (data1.ReturnValue == "1" || data1.ReturnValue=='2' ) {
        insUpdOrder(data1.UserDetailID);
    }
}
function insUpdOrder(){
    var MasterData = {
        
        "OrderID":'0',
        "UserID":Cookies.get('permUserId'),
        "Subtotal": $('#subtotaltocheckoutbill').text().replace('₹',''),
        "ShippingCharge":$('#shippingcosttocheckoutbill').text().replace('₹',''),
        "TotalAmount":$('#grandtotaltocheckoutbill').text().replace('₹',''),
        "PaymentMode": $('input[name="paymentMode"]:checked').val(),
        "Address":$('#useraddress').val(),
        "OrderDetails":[],
        "Status":'Pending',
        "ReferenceID": Cookies.get('referralID') ? Cookies.get('referralID') : 0
    };
    var details = sessionStorage.OrderDetails;
    details=JSON.parse(details).recordset;
    //console.log(details);
    for(var i = 0; i < details.length; i++){
        var detail = details[i];
        //console.log(detail);
        var orderdetail= {
            "PRODUCT_ID":detail.Product_ID,
            "QUANTITY":detail.Quantity
        }
        MasterData.OrderDetails.push(orderdetail);
    }
    MasterData = JSON.stringify(MasterData);
    var path = serverpath + "Orders";
    ajaxpost(path, 'ordersCreate', 'comment', MasterData, 'control');
}
function ordersCreate(data){
    data=JSON.parse(data);
    data1=data.recordset[0];
    if(data1.ReturnValue=='1'){
        sessionStorage.OrderID=data1.OrderID;
        sessionStorage.Type="";
        window.location='/User/Invoice';
    }
}

function alphabet(){
    $('#firstname,#lastname').keypress(function (e) {
        var k = e.which;
        var ok = k >= 65 && k <= 90 || // A-Z
        k >= 97 && k <= 122||  // a-z
        k == 46;
        if (!ok){
            e.preventDefault();
        }
    });
}



function FetchUserDetail(){
    var path = serverpath + "user/"+Cookies.get('permUserId')+"";
    ajaxget(path, 'parsrdataFetchUserDetail', 'comment','control')
}
function parsrdataFetchUserDetail(data){
    data = JSON.parse(data)
    data1=data.recordset;
  
       for(var i=0; i<data1.length; i++){
         $("#firstname").val(data1[i].FirstName)
         $("#lastname").val(data1[i].LastName)
         $("#emailaddress").val(data1[i].EmailAddress)
         $("#mobilenumberuser").val(data1[i].Telephone)
         $("#useraddress").val(data1[i].Address)
         $("#City").html('Bhopal')
         $("#postcode").val(data1[i].PostalCode)

         Cookies.set('username',data1[i].FirstName + ' ' + data1[i].LastName,{ expires: 3600 * 1000 * 24 * 365, path: '/' });
   }   
                 
}
