function SendOTP(flag){
    if($('#mobilenumber').val()=='9755377626'){
        if(flag=='Verify'){
        $('#otpDiv').show();
        $('#mobileDiv').hide();
        }
        if(flag=='Match'){
            if($('#otptext').val()=='9755377626'){
                sessionStorage.Admin='Y';
                toastr.success('Login Successfully');
                $('#loginModal').modal('toggle');
                FetchMenu();
            }
            else{
                toastr.warning('Please Enter Correct OTP');
                return false;
            }
        }
    }else{
        var otp='';

        if(flag=='Verify'){
            otp =generateOTP();
            sessionStorage.otp=otp;
            sessionStorage.MobileNumber=$('#mobilenumber').val();
        }
        if(flag=='Match'){
            otp=$('#otptext').val();
        }
     var MasterData = {
     
      "MobileNumber":$('#mobilenumber').val()==""? sessionStorage.MobileNumber:$('#mobilenumber').val(),
      "Otp":otp,
      "Flag":flag
        
 };
 MasterData = JSON.stringify(MasterData)
 var path = serverpath + "MobileVerification";
 ajaxpost(path, 'mobileVerification', 'comment', MasterData, 'control');
 
    }

}
function mobileVerification(data){
   data = JSON.parse(data)
   var data1=data.recordset[0]
 
     if (data1.ReturnValue == "1") {
         var msg ="Welcome to Pustakien use the otp "+ sessionStorage.otp +" to verify your contact number and login to Pustakien account. Please do not share with anyone."
        sendmsg(msg, $('#mobilenumber').val());
            toastr.success('OTP has been sent');
            $('#otpDiv').show();
            $('#mobileDiv').hide();
            return true;
        }
    else if (data1.ReturnValue == "2") {
        insupdnewUserDetail();
        FetchMenu() ;
        return true;      
        }
     else if(data1.ReturnValue=="3"){
         toastr.warning('Please Enter Correct OTP');
            return false;
                 }
}
function insupdnewUserDetail(){
    var MasterData = {
    
        "UserDetailID":'0',
        "FirstName":'Guest',
        "LastName":'',
        "EmailAddress":'',
        "Address":'',
        "StreetAddress2":'0',
        "City":'1',
        "State":'0',
        "PostalCode":'',
        "Country":"0",
        "Telephone":$('#mobilenumber').val(),
        "TempUserID":Cookies.get('tempUser')
       
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "user";
ajaxpost(path, 'newuserDetail', 'comment', MasterData, 'control')

}
function newuserDetail(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
      if (data1.ReturnValue == "1" || data1.ReturnValue=='2' ) {
        Cookies.set('permUserId', data1.UserDetailID,{ expires: 3600 * 1000 * 24 * 365, path: '/' });
        $('#loginModal').modal('toggle');
        toastr.success('Login Successful');
       }
 }