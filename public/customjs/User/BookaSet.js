function FillSchoolBoard(control){
    var path = serverpath + "boardmaster/0/1"
    ajaxget(path,'boardDdl','comment',control);
  
}
function boardDdl(data,control){
    data = JSON.parse(data)
 
    var data1=data.recordset;
        jQuery("#board").empty();
        jQuery("#board").append(jQuery("<option ></option>").val("0").html("Select Board"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#board").append(jQuery("<option></option>").val(data1[i].BoardID).html(data1[i].BoardName));
        }
    $('#boarddl').addClass('nice-select');
}


function FillClassByBoard(boardid){
    var path = serverpath + "BoardClass/"+boardid+"/1"
    ajaxget(path,'classDdl','comment','control');
  
}
function classDdl(data,control){
    data = JSON.parse(data)
 
    var data1=data.recordset;
        jQuery("#classddl").empty();
        jQuery("#classddl").append(jQuery("<option ></option>").val("0").html("Select Class"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#classddl").append(jQuery("<option></option>").val(data1[i].ClassID).html(data1[i].ClassName));
        }
        $("#classddl").addClass('nice-select');
}
function FillSchoolByBoard(boardid){
    var path = serverpath + "SchoolMaster/0/"+boardid+""
    ajaxget(path,'schoolDdl','comment','control');
  
}
function schoolDdl(data,control){
    data = JSON.parse(data)
 
    var data1=data.recordset;
        jQuery("#schoolddl").empty();
        jQuery("#schoolddl").append(jQuery("<option ></option>").val("0").html("Select School"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#schoolddl").append(jQuery("<option></option>").val(data1[i].SchoolID).html(data1[i].SchoolName));
        }
        $("#schoolddl").addClass('nice-select');
}
function FillSubject(){
   if(jQuery("#classddl").val()=='15' ||jQuery("#classddl").val()=='16') {
    var path = serverpath + "HighSecondarySubject/0"
    ajaxget(path,'subjectDdl','comment','control');
   }
   else{
    $("#subjectDiv").hide();
   }
 
  
}
function subjectDdl(data,control){
    data = JSON.parse(data)
 
    var data1=data.recordset;
        jQuery("#subjectddl").empty();
        jQuery("#subjectddl").append(jQuery("<option ></option>").val("0").html("Select Subject Set"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#subjectddl").append(jQuery("<option></option>").val(data1[i].ID).html(data1[i].SubjectName));
        }
        $("#subjectddl").addClass('nice-select');
        $("#subjectDiv").show();
}
function checkValidiation(){
    if($.trim($('#username').val())==''){
        toastr.warning('Please Enter Your Name');
        return false;
    }
if($.trim($('#mobilenumberenquiry').val())==''){
    toastr.warning('Please Enter Your Mobile Number');
    return false;
}
if($.trim($('#mobilenumberenquiry').val())!='' && $.trim($('#mobilenumberenquiry').val()).length!=10){
    toastr.warning('Please Enter Correct Mobile Number');
    return false;
}
if($('#board').val()=='0'){
    toastr.warning('Please Select Board');
    return false;
}
if($('#schoolddl').val()=='0'){
    toastr.warning('Please Select School');
    return false;
}
if($('#classddl').val()=='0'){
    toastr.warning('Please Select Class');
    return false;
}
else{
    if(sessionStorage.Admin != 'Y')
    {
      var otp =generateOTP();
        sessionStorage.bookasetotp=otp;
        var msg ="Use the otp "+ sessionStorage.bookasetotp +" to verify your mobile number. Please do not share with anyone."
        sendmsg(msg, $('#mobilenumberenquiry').val());
        toastr.success("OTP has been sent to your mobile number");
$('#bookasetVerificationModal').modal('toggle');
    }
    else{
        insUpdEnquiry();
    }
   
}
}
function insUpdEnquiry(){
    var MasterData = {
        
        "UniqueID":'0',
        "BaordID":$('#board').val(),
        "SchoolID":$('#schoolddl').val(),
        "ClassID":$('#classddl').val(),
        "Name":$('#username').val(),
        "MobileNumber":$('#mobilenumberenquiry').val(),
        "ReferenceID": Cookies.get('referralID') ? Cookies.get('referralID') : 0,
        "Subject":$('#subjectddl').val()==null ||$('#subjectddl').val()==undefined||$('#subjectddl').val()==''?0:$('#subjectddl').val()
       
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "bookaset";
ajaxpost(path, 'bookASet', 'comment', MasterData, 'control')

}
function bookASet(data){
   data = JSON.parse(data)
   var data1=data.recordset[0]
   if (data1.ReturnValue == "0") {
       toastr.warning('You already ordered this set of book')
   }

     if (data1.ReturnValue == "1") {
         var mblno=$('#mobilenumberenquiry').val()
        var msg =`Thank you. Your request has been sent successfully. We will get back to you as soon as possible. (www.pustakein.com)`
        sendmsg(msg,mblno);

  BookASet=`<tr><td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;"><b> BOOK A SET</b></td></tr>
  <tr><td style="padding: 20px 0 30px 0; color: #153643; font-family: auto; font-size: 16px; line-height: 20px;"><table style="width:100%"><tr><th style="width:15%;text-align: left;">Name :</th><td>`+$('#username').val()+`</td></tr>   <tr><th style="width:15%;text-align: left;">Phone no. :</th><td>`+$('#mobilenumberenquiry').val()+`</td></tr> <tr> <th style="width:15%;text-align: left;">Board :</th><td>`+$('#board option:selected').html()+`</td></tr> <tr> <th style="width:15%;text-align: left;">School :</th><td>`+$('#schoolddl option:selected').html()+`</td></tr> <tr> <th style="width:15%;text-align: left;">Class :</th><td>`+$('#classddl option:selected').html()+`</td></tr>  </table></td></tr>`
   
  sendMail('pustakeinapp@gmail.com',BookASet,'Book A Set')

  
    //   toastr.success('Enquiry sent successfully');
    //   return true;
    reset()
  setTimeout(function(){ $('#bookasetModal2').modal('toggle'); });

        }
}


function reset(){
    jQuery('#username').val('') 
    jQuery('#mobilenumberenquiry').val('')
    jQuery('#board').val('0')
    jQuery('#schoolddl').val('0')
    jQuery('#classddl').val('0')
    jQuery('#subjectddl').val('0')
}
function verifyMobileNumber(){
    if($('#bookasetotptext').val()!=''){
        if($('#bookasetotptext').val()==sessionStorage.bookasetotp){
            $('#bookasetVerificationModal').modal('toggle');
            insUpdEnquiry();
        }
        else{
            toastr.warning("Please enter correct OTP");
            return false;

        }
    }
    else{
        toastr.warning("Please enter OTP");
        return false;
    }
    
}
function checkAvailibility(){
     var path = serverpath + "checkAvailibility/"+$('#board').val()+"/"+$('#schoolddl').val()+"/"+$('#classddl').val()+""
     ajaxget(path,'checkBookASet','comment','control');
   }
 function checkBookASet(data,control){
     data = JSON.parse(data)
  
     var data1=data.recordset[0];
     if (data1.ReturnValue == "1") {
         $("#booksetStatus").text('Bookset Available');
         $('#booksetStatus').css({'color':'green'}); 
         $('#btnSubmit').text('Book Now');
    }
 
      if (data1.ReturnValue == "2") {
        $("#booksetStatus").text('Bookset Not Available')
        $('#booksetStatus').css({'color':'orange'});
        $('#btnSubmit').text('Notify Me');
      }

 }