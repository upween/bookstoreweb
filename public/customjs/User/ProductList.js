
    function fetchProductList() {
        var path = serverpath + "boardclasswiseproduct/"+sessionStorage.BoardID+"/"+sessionStorage.ClassID+"/1";
       ajaxget(path,'createProductCards','comment','control');
    }
    function createProductCards(data,control){ 
    data=JSON.parse(data);
    data1=data.recordset;
    var appenddata='';
    $('#productcards').empty();
    if(data.recordset.length){
        $('#head').html(data1[0].BoardName +' - '+ data1[0].ClassName);
    for(var i=0 ; i < data1.length ; i++){
        appenddata += `
        <div class="col-lg-4 col-sm-6">
            <div class="product-card">
                <div class="product-grid-content">
                    <div class="product-header">
                       
                        <h3><a href='#' data-toggle="modal" data-target="#quickModal" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;"
                           title='`+data1[i].ProductName+`'     onclick=fillModal(`+data1[i].ProductID+`,'`+encodeURI(data1[i].Image1)+`','`+encodeURI(data1[i].Language)+`','`+encodeURI(data1[i].ProductName)+`','`+encodeURI(data1[i].Price)+`','`+encodeURI(data1[i].Availability)+`','`+encodeURI(data1[i].ISBNNO)+`','`+encodeURI(data1[i].Publisher)+`','`+encodeURI(data1[i].NoOfPages)+`','`+encodeURI(data1[i].Binding)+`','`+encodeURI(data1[i].Imprint)+`','`+encodeURI(data1[i].Edition)+`','`+encodeURI(data1[i].Condition)+`','`+encodeURI(data1[i].SubjectName)+`','`+encodeURI(data1[i].ProductDescription)+`') >
                        ` + data1[i].ProductName + `</a></h3>
                    </div>
                <div class="product-card--body">
                    <div class="card-image">
                        <img src="`+productimgpath + data1[i].Image1+`" alt="">
                        <div class="hover-contents">
                            
                            <div class="hover-btns">
                             
                                <a href="#" data-toggle="modal" data-target="#quickModal"
                                onclick=fillModal(`+data1[i].ProductID+`,'`+encodeURI(data1[i].Image1)+`','`+encodeURI(data1[i].Language)+`','`+encodeURI(data1[i].ProductName)+`','`+encodeURI(data1[i].Price)+`','`+encodeURI(data1[i].Availability)+`','`+encodeURI(data1[i].ISBNNO)+`','`+encodeURI(data1[i].Publisher)+`','`+encodeURI(data1[i].NoOfPages)+`','`+encodeURI(data1[i].PublicationYear)+`','`+encodeURI(data1[i].Binding)+`','`+encodeURI(data1[i].Imprint)+`','`+encodeURI(data1[i].Edition)+`','`+encodeURI(data1[i].Condition)+`','`+encodeURI(data1[i].SubjectName)+`','`+encodeURI(data1[i].ProductDescription)+`') 
                                    class="single-btn">
                                    <i class="fas fa-eye"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="price-block">
                        <span class="price">₹ ` + data1[i].Price + `</span>
                        <del class="price-old"  style="display:none">£51.20</del>
                        <span class="price-discount" style="display:none">20%</span>
                    </div>
                </div>
            </div>
         
        </div>
    </div>`;
    
    }
    $('#productcards').append(appenddata);
}
else{
    $('#head').html('No Books Found!');
}
    
}

function fetchCategories() {
    var path = serverpath + "rolemenu/1"
   ajaxget(path,'fillCategorySidebar','comment','control');
}
function fillCategorySidebar(data,control){  
    data = JSON.parse(data);
    var data1=data.recordset;
    var data2=data.recordset;

    for(var i=0; i<data1.length; i++){
        if(data1[i].ParentID==0){

            var mainmenu ='    <li class="boardcategories"  data-value="' +data1[i].MasterID+ '">'
                      +'   <a href="javascript:void(0)">'+data1[i].MenuName+'</a>'
                      +'    <ul class="inner-cat-items" id="classCateogories' + data1[i].MenuID + '"></ul></li>';
            $('#boardcategories').append(mainmenu);
                      
        } 
    }

    for(var i=0; i<data1.length; i++){
        var parentID = data1[i].ParentID;
        if(parentID == 0){
            continue;
        }

        var subMenu ='    <li  onclick="setsessionfilter('+data1[i].MasterID+','+$('ul#classCateogories' + parentID).closest('.boardcategories').attr('data-value')+')">'
                    +'   <a href="javascript:void(0)">'+data1[i].MenuName+'</a></li>';
        $('ul#classCateogories' + parentID).append(subMenu);
    }

    $("ul[id^='classCateogories'].inner-cat-items").each(function() {
        if($(this).find('li').length == 0) {
            $(this).closest('.boardcategories').hide();
        }
    });
}
function setsessionfilter(classid,boardid){
    sessionStorage.ClassID=classid;
    sessionStorage.BoardID=boardid;
    fetchProductList();
    }
   