function fetchUserCart() {
    var userId=Cookies.get('permUserId')?Cookies.get('permUserId'):Cookies.get('tempUser');
    var path = serverpath + "usercart/"+userId+"";
   ajaxget(path,'fillCartItems','comment','control');
};
function fillCartItems(data,control){ 
data=JSON.parse(data);
data1=data.recordset;
var appenddata='';
var subtotal=0;
$('#carttablebody').empty();
if(data.recordset.length){

for(var i=0 ; i < data1.length ; i++){
    var total=0;
    total=parseInt(data1[i].Price.replace('?',''))*parseInt(data1[i].Quantity);
    appenddata += 
`<tr>
<td class="pro-remove"><a><i class="far fa-trash-alt" onclick= deleteCartItem(`+data1[i].UniqueID+`)></i></a>
</td>
<td class="pro-thumbnail"><a href="#"><img
            src="`+productimgpath + data1[i].Image1 +`" alt="Product"></a></td>
<td class="pro-title"><a href="#">`+data1[i].ProductName +`</a></td>
<td class="pro-price"><span>₹ `+data1[i].Price.replace('?','') +`</span></td>
<td class="pro-quantity">
    <div class="pro-qty">
        <div class="count-input-block">
            <input type="number" min="1" class="form-control text-center" id="quantitytd`+data1[i].UniqueID+`" 
                value="`+ data1[i].Quantity +`" onfocusout=setTotal('`+encodeURI(data1[i].Price.replace('?','')) +`',`+data1[i].UniqueID+`)>
        </div>
    </div>
</td>
<td class="pro-subtotal"><span id="totaltd`+data1[i].UniqueID+`">₹ `+ total +`</span></td>

</tr>`;
subtotal +=total;

}
$('#carttablebody').append(appenddata);
$('#subtotaltocheckout').html('₹'+subtotal);
var shipping=0;
var grandtotal=0;
if(subtotal>2000 || subtotal==0){
    $('#shippingcosttocheckout').html('₹ 0');
}
else{
    shipping=200;
    $('#shippingcosttocheckout').html('₹ 200');
}
 grandtotal=subtotal+shipping;

$('#grandtotaltocheckout').html('₹'+grandtotal);
}
};


function setTotal(price,id){
    if($.trim($("#quantitytd" + id).val())=="0" || $.trim($("#quantitytd" + id).val()) ==""){
        toastr.warning('Please Select Proper Quantity',);
        return false;
    }
        else{
          
    
    var preprice=$('#totaltd'+id).text().replace('₹','');
    var quantity=$("#quantitytd" + id).val();
    var total=0;
    total=parseInt(decodeURI(price))*parseInt(quantity);
    $('#totaltd'+id).html('₹ '+total);

// for checkout
    var subtotal=$('#subtotaltocheckout').text().replace('₹','');
    var  parseSubtotal=parseInt(subtotal);
    var diffSubtotal=parseSubtotal-parseInt(preprice);
    var newSubtotal=diffSubtotal+total;
    $('#subtotaltocheckout').html('₹'+newSubtotal);
    var shipping=0;
    var grandtotal=0;
    if(newSubtotal>2000){
        $('#shippingcosttocheckout').html('₹ 0');
    }
    else{
        shipping=200;
        $('#shippingcosttocheckout').html('₹ 200');
    }
 grandtotal=newSubtotal+shipping;

    $('#grandtotaltocheckout').html('₹'+grandtotal);
        updCart(id,quantity);
   
   
    }
}
function deleteCartItem(id){
 
        var userId=Cookies.get('permUserId')?Cookies.get('permUserId'):Cookies.get('tempUser');
        var path = serverpath + "deletecartitems/'"+userId+"'/'"+id+"'";
       ajaxget(path,'removeCartItems','comment','control');
    };
    function removeCartItems(data,control){ 
    data=JSON.parse(data);
    var data1=data.recordset[0]
     
         if (data1.ReturnValue == "1") {
                fetchUserCart();
              }
};
function updCart(id,quantity){
       
    var MasterData = {
    
        "UniqueID":id,
        "UserID":'0',
        "Product_ID":'0',
        "Quantity":quantity,
        "EntryFrom":'0',
        "Price":'0',
        "DiscountedPrice":'0',
       
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "usercart";
ajaxpost(path, 'updToCart', 'comment', MasterData, 'control')

}
function updToCart(data){
   data = JSON.parse(data)
   var data1=data.recordset[0]
 
     if (data1.ReturnValue == "2") {
           // window.location='/User/Cart';
           //toastr.success('Update','success')
        }
}