function FillVendors(){
    var path = serverpath + "vendormaster/0"
    ajaxget(path,'parsedataFillVendors','comment','control');
  
}
function parsedataFillVendors(data){
    data = JSON.parse(data)
    data1=data.recordset;

        jQuery("#vendorname").empty();
        jQuery("#vendorname").append(jQuery("<option ></option>").val("0").html("Select Vendor"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#vendorname").append(jQuery("<option></option>").val(data1[i].VendorID).html(data1[i].FirstName+' '+data1[i].LastName));
        }
 $('#vendorname').addClass('nice-select');
 
 
}



function FillSchoolBoard(){
    var path = serverpath + "boardmaster/0/1"
    ajaxget(path,'SchoolBoard','comment','control');
  
}
function SchoolBoard(data){
      data = JSON.parse(data)
    data1=data.recordset;

        jQuery("#boardddl").empty();
        jQuery("#boardddl").append(jQuery("<option ></option>").val("0").html("Select Board"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#boardddl").append(jQuery("<option></option>").val(data1[i].BoardID).html(data1[i].BoardName));
        }
    $('#boardddl').addClass('nice-select');
}


function FillSchoolByBoard(boardid){
    var path = serverpath + "SchoolMaster/0/"+boardid+""
    ajaxget(path,'SchoolByBoard','comment','control');
  
}
function SchoolByBoard(data,control){
    data = JSON.parse(data)
    data1=data.recordset;

        jQuery("#schoolddl").empty();
        jQuery("#schoolddl").append(jQuery("<option ></option>").val("0").html("Select School"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#schoolddl").append(jQuery("<option></option>").val(data1[i].SchoolID).html(data1[i].SchoolName));
        }
       $("#schoolddl").addClass('nice-select');
}


function FillClassByBoard(boardid){
    var path = serverpath + "BoardClass/"+boardid+"/1"
    ajaxget(path,'classDdl','comment','control');
  
}
function classDdl(data,control){
    data = JSON.parse(data)
 
    var data1=data.recordset;
        jQuery("#classddl").empty();
        jQuery("#classddl").append(jQuery("<option ></option>").val("0").html("Select Class"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#classddl").append(jQuery("<option></option>").val(data1[i].ClassID).html(data1[i].ClassName));
        }
        $("#classddl").addClass('nice-select');
}


function checkValidiation(){
    if($.trim($('#vendorname').val())==''){
        toastr.warning('Please Select Vendor Name');
        return false;
    }
    if($.trim($('#boardddl').val())==''){
        toastr.warning('Please Select Board');
        return false;
    }
    if($.trim($('#schoolddl').val())==''){
        toastr.warning('Please Select School');
        return false;
    }
    if($.trim($('#classddl').val())==''){
        toastr.warning('Please Select Class');
        return false;
    }
    if($.trim($('#sellingprice').val())==''){
        toastr.warning('Please Enter Price');
        return false;
    }
    else{
        insupdVendorBookSet()
    }
}

function insupdVendorBookSet(){
    var chkval = $("#checkbox")[0].checked
    if (chkval == true){
        chkval = "1"
    }else{
        chkval="0"
    }
    var MasterData = {
        
        "UniqueID":'0',
        "VendorID":$('#vendorname').val(),
        "BoardID":$('#boardddl').val(),
        "SchoolID":$('#schoolddl').val(),
        "ClassID":$('#classddl').val(),
        "Price":$('#sellingprice').val(),
        "IsActive":chkval
       
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "VendorSetMapping";
ajaxpost(path, 'VendorBookSet', 'comment', MasterData, 'control')
}

function VendorBookSet(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
  
      if (data1.ReturnValue == "1") {

        reset()
        toastr.success('Submit successfully');
        return true;
      }
     else if (data1.ReturnValue == "0") {
        reset()
        toastr.success('Detail already Exists ');
      return true;
        }
    }

    function reset(){
    
        $('#vendorname').val('0'),
        $('#boardddl').val('0'),
        $('#schoolddl').val('0'),
        $('#classddl').val('0'),
        $('#sellingprice').val(''),
        $("#checkbox"). prop("checked", false);
    }
