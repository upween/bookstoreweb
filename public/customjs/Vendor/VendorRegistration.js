function checkValidiation(){
    if($.trim($('#vendorfirstname').val())==''){
        toastr.warning('Please Enter First Name');
        return false;
    }
    if($.trim($('#vendorlastname').val())==''){
        toastr.warning('Please Enter Last Name');
        return false;
    }
    if($.trim($('#vendormobilenumberuser').val())==''){
        toastr.warning('Please Enter Mobile No.');
        return false;
    }
    if($.trim($('#vendormobilenumberuser').val())!='' && $.trim($('#vendormobilenumberuser').val()).length!=10){
        toastr.warning('Please Enter Correct Mobile Number');
        return false;
    }
    if($.trim($('#vendoremailaddress').val())==''){
        toastr.warning('Please Enter Email Address');
        return false;
    }
    if($.trim($('#vendoraddress').val())==''){
        toastr.warning('Please Enter Address');
        return false;
    }
    if($.trim($('#discountpercentage').val())==''){
        toastr.warning('Please Enter Discount Percentage');
        return false;
    }
    else{
        insUpdVender()
    }
}


function insUpdVender(){
    var chkval = $("#checkbox")[0].checked
    if (chkval == true){
        chkval = "1"
    }else{
        chkval="0"
    }

    var MasterData = {
        
        "VendorID":sessionStorage.getItem('VendorID',data1[i].VendorID),
        "FirstName":$('#vendorfirstname').val(),
        "LastName":$('#vendorlastname').val(),
        "MobileNumber":$('#vendormobilenumberuser').val(),
        "Email":$('#vendoremailaddress').val(),
        "Address":$('#vendoraddress').val(),
        "DiscontPercentage":$('#discountpercentage').val() ,
        "Active_YN":chkval
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "vendormaster";
ajaxpost(path, 'parsedatainsUpdVender', 'comment', MasterData, 'control')
}

function parsedatainsUpdVender(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
  
      if (data1.ReturnValue == "1") {
        reset()
        FillVendors()
      toastr.success('Vendor Create successfully');
      return true;
      }
        if (data1.ReturnValue == "2") {
            reset()
            FillVendors()
          toastr.success('Update successfully');
      return true;
      }
    }


    function reset(){
        $('#vendorfirstname').val('') 
        $('#vendorlastname').val('')
        $('#vendormobilenumberuser').val('')
        $('#vendoremailaddress').val('')
        $('#vendoraddress').val('')
        $('#discountpercentage').val('')
        $("#checkbox"). prop("checked", false);
    }


    function FillVendors(){
        var path = serverpath + "vendormaster/0"
        ajaxget(path,'parsedataFillVendors','comment','control');
      
    }
    function parsedataFillVendors(data){
        data = JSON.parse(data)
        data1=data.recordset;
    
        var VENDOR='';
        $('#vendordetail').empty();
            for (var i = 0; i < data1.length; i++) {
                VENDOR+='<tr class="i"><td>'+[i+1]+'</td><td>'+data1[i].FirstName+' '+data1[i].LastName+'</td><td>'+data1[i].MobileNumber+'</td><td>'+data1[i].Email+'</td><td>'+data1[i].Address+'</td><td>'+data1[i].DiscontPercentage+' %</td><td><button class="btn btn--primary w-100" onclick=Edit("'+data1[i].VendorID+'","'+encodeURI(data1[i].FirstName)+'","'+encodeURI(data1[i].LastName)+'","'+encodeURI(data1[i].MobileNumber)+'","'+encodeURI(data1[i].Email)+'","'+encodeURI(data1[i].Address)+'","'+encodeURI(data1[i].DiscontPercentage)+'") >Edit </button></td></tr>'
            }
        $('#vendordetail').append(VENDOR);
        $('#vendordetail').clientSidePagination();
    }
   

    function Edit(VendorID,FirstName,LastName,MobileNumber,Email,Address,DiscontPercentage){
        window.scrollTo(0, 1000);
        sessionStorage.setItem("VendorID",VendorID)
        $("#vendorfirstname").val(decodeURI(FirstName))
        $("#vendorlastname").val(decodeURI(LastName))
        $("#vendormobilenumberuser").val(decodeURI(MobileNumber))
        $("#vendoremailaddress").val(decodeURI(Email))
        $("#vendoraddress").val(decodeURI(Address))
        $("#discountpercentage").val(decodeURI(DiscontPercentage))
    }
															
												
														