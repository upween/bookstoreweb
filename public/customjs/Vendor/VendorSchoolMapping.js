function FillVendors(){
    var path = serverpath + "vendormaster/0"
    ajaxget(path,'parsedataFillVendors','comment','control');
  
}
function parsedataFillVendors(data){
    data = JSON.parse(data)
    data1=data.recordset;

        jQuery("#vendorname").empty();
        jQuery("#vendorname").append(jQuery("<option ></option>").val("0").html("Select Vendor"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#vendorname").append(jQuery("<option></option>").val(data1[i].VendorID).html(data1[i].FirstName+' '+data1[i].LastName));
        }
 $('#vendorname').addClass('nice-select');
 
 
}



function FillSchoolBoard(){
    var path = serverpath + "boardmaster/0/1"
    ajaxget(path,'SchoolBoard','comment','control');
  
}
function SchoolBoard(data){
      data = JSON.parse(data)
    data1=data.recordset;

        jQuery("#boardname").empty();
        jQuery("#boardname").append(jQuery("<option ></option>").val("0").html("Select Board"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#boardname").append(jQuery("<option></option>").val(data1[i].BoardID).html(data1[i].BoardName));
        }
    $('#boardname').addClass('nice-select');
}


function FillSchoolByBoard(boardid){
    var path = serverpath + "SchoolMaster/0/"+boardid+""
    ajaxget(path,'SchoolByBoard','comment','control');
  
}
function SchoolByBoard(data,control){
    data = JSON.parse(data)
    data1=data.recordset;

        jQuery("#schoolname").empty();
        jQuery("#schoolname").append(jQuery("<option ></option>").val("0").html("Select School"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#schoolname").append(jQuery("<option></option>").val(data1[i].SchoolID).html(data1[i].SchoolName));
        }
       $("#schoolname").addClass('nice-select');
}




function checkValidiation(){
    if($.trim($('#vendorname').val())==''){
        toastr.warning('Please Select Vendor Name');
        return false;
    }
    if($.trim($('#boardname').val())==''){
        toastr.warning('Please Select Board');
        return false;
    }
    if($.trim($('#schoolname').val())==''){
        toastr.warning('Please Select School');
        return false;
    }
    else{
        insUpdVenderMapping()
    }
}

function insUpdVenderMapping(){
    var chkval = $("#checkbox")[0].checked
    if (chkval == true){
        chkval = "1"
    }else{
        chkval="0"
    }
    var MasterData = {
        
        "UniqueID":sessionStorage.getItem('UniqueID',data1[i].UniqueID),
        "VendorID":$('#vendorname').val(),
        "BoardID":$('#boardname').val(),
        "SchoolID":$('#schoolname').val(),
        "IsActive":chkval
       
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "VendorSchoolMapping";
ajaxpost(path, 'vandermapping', 'comment', MasterData, 'control')
}

function vandermapping(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
  
      if (data1.ReturnValue == "1") {

        reset()
        FetchMappedVendor()
        toastr.success('Vendor Mapped successfully');
        return true;
      }
     else if (data1.ReturnValue == "0") {
        reset()
        toastr.success('Vendor Allready Mapped');
      return true;
        }
        else  if (data1.ReturnValue == "2") {
            reset()
            FetchMappedVendor()
            toastr.success('Update successfully');
            return true;
          }
    }

    function reset(){
        $('#vendorname').val('0'),
        $('#boardname').val('0'),
        $('#schoolname').val('0'),
        $("#checkbox"). prop("checked", false);
    }

    function FetchMappedVendor(){
        var path = serverpath + "VendorSchoolMapping/0/0/0"
        ajaxget(path,'Vender','comment','control');
      
    }
    function Vender(data,control){
        data = JSON.parse(data)
     
        var data1=data.recordset;
          var VENDORS=''
        for(var i=0; i<data1.length; i++){

            VENDORS+=`<tr class='i'><td>`+[i+1]+`</td><td>`+data1[i].FirstName+` `+data1[i].LastName+`</td><td>`+data1[i].BoardName+`</td><td>`+data1[i].SchoolName+`</td><td><button class="btn btn--primary w-100" onclick=Edit("`+data1[i].UniqueID+`","`+data1[i].VendorID+`","`+data1[i].BoardID+`","`+sessionStorage.SchoolId+`") >Edit </button></td></tr>`
        }
        $('#tbodydetail').append(VENDORS);
        $('#tbodydetail').clientSidePagination();
    }


    function Edit(UniqueID,Name,Board,School){
        window.scrollTo(0, 1000);
        sessionStorage.setItem("UniqueID",UniqueID)
        $("#vendorname").val(decodeURI(Name))
        $("#boardname").val(decodeURI(Board))
        $("#schoolname").val(decodeURI(School))
     //   $("#checkbox"). prop("checked", false);
    }
															
			