var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var port = process.env.PORT || 9337; 
var routes = require('./routes/index');
var session = require('express-session')
var multer = require('multer');
var cors = require('cors');
var Loki = require('lokijs');
var bodyParser = require('body-parser');
const helmet = require('helmet');

var app = express();
app.use(helmet.frameguard({ action: "deny" }));
app.use(helmet.xssFilter())
app.use(helmet.noCache())
app.use(helmet.noSniff())
// view engine setup
app.use(helmet.xssFilter())
app.use(helmet.noCache())
app.use(helmet.noSniff())
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret: 'rojgarclient'}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname,'uploads')));
app.use(express.static('uploads'));
app.all('/uploads/*', function (req,res, next) {

  if (req.session.login){
    res.status(200).send();
  }
  else{
    res.status(403).send({
      message: 'Access Forbidden'
    });
  }
  
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
routes(app);
var storage = multer.diskStorage({
  
  destination: function (req, file, cb) {
    if (req.cookies.modaltype == "image"){
      cb(null, './uploads/product/')
    }
   
  },
  filename: function (req, file, cb) {
    if (req.cookies.modaltype == "image"){
      cb(null, 'Product'+req.cookies.ProductID+Date.now()+path.extname(file.originalname))
    }
  
  }
});
// Init Upload
const upload = multer({
  storage: storage,
  limits:{fileSize: 1000000},
  fileFilter: function(req, file, cb){
    checkFileType(req,file, cb);
  }
}).array('myFiles', 3);

function checkFileType(req,file, cb){
  // Allowed ext
  var ext = path.extname(file.originalname);
  if (req.cookies.modaltype == "image"){
	  if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            cb('Error: Image Only!');
        }
		else{
		return cb(null,true);}
  }

}

app.post('/Admin/AddProduct', (req, res) => {
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.files == undefined || req.files.length == 0){
        res.end("Error No File Selected");
      } else {
        var fileNames = [];
        for(var i = 0; i < req.files.length; i++) {
          fileNames.push(req.files[i].filename);
        }
        res.end('File(s) Uploaded!'+fileNames.join(';'));
      }
    }
  });
}); 

app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  //res.render('pages/error');
});
app.listen(port, function() {
  console.log('Server listening on port ' + port + '...');
});
module.exports = app;
