
module.exports = function(app) {
  app.get('/', function(req, res) {

        if(req.query.r){
      if(req.query.r!=null ||req.query.r!=undefined){

        res.cookie('referralID',req.query.r);
      
      }
}
    res.render('index');
  });
  app.get('/Conditions', function(req, res) {
    res.render('pages/Footer/Conditions.ejs');
  });
  app.get('/ShippingPolicy', function(req, res) {
    res.render('pages/Footer/ShippingPolicy.ejs');
  });
  app.get('/PrivacyPolicy', function(req, res) {
    res.render('pages/Footer/PrivacyPolicy.ejs');
  });
  app.get('/OrdersandReturn', function(req, res) {
    res.render('pages/Footer/OrdersandReturn.ejs');
  });
  app.get('/CancellationPolicy', function(req, res) {
    res.render('pages/Footer/CancellationPolicy.ejs');
  });
  app.get('/User/Login-Register', function(req, res) {
    res.render('pages/User/Login-Register.ejs');
  });
  app.get('/Admin/AddProduct', function(req, res) {
    res.render('pages/Admin/AddProduct.ejs');
  });
  app.get('/PaymentOption', function(req, res) {
    res.render('pages/Footer/PaymentOption.ejs');
  });
  app.get('/TermsandConditions', function(req, res) {
    res.render('pages/Footer/TermsandConditions.ejs');
  });
  app.get('/Contactus', function(req, res) {
    res.render('pages/Footer/Contactus.ejs');
  });
  app.get('/User/ProductList', function(req, res) {
    res.render('pages/User/ProductList.ejs');
  });
  app.get('/User/Cart', function(req, res) {
    res.render('pages/User/Cart.ejs');
  });
  app.get('/User/Checkout', function(req, res) {
    res.render('pages/User/Checkout.ejs');
  });
  app.get('/BooksEnquiry', function(req, res) {
    res.render('pages/Footer/BooksEnquiry.ejs');
  });
  app.get('/User/BookASet', function(req, res) {

    res.render('pages/User/BookASet.ejs');
  });
  app.get('/Admin/CreateOrder', function(req, res) {
    res.render('pages/Admin/CreateOrder.ejs');
  });
  app.get('/User/Invoice', function(req, res) {
    res.render('pages/User/Invoice.ejs');
  });
  app.get('/User/Dashboard', function(req, res) {
    res.render('pages/User/Dashboard.ejs');
  });
  app.get('/Admin/Records', function(req, res) {
    res.render('pages/Admin/Records.ejs');
  });
  app.get('/Admin/Userlogindetail', function(req, res) {
    res.render('pages/Admin/Userlogindetail.ejs');
  });
  app.get('/Vendor/Registration', function(req, res) {
    res.render('pages/Vendor/VendorRegistration.ejs');
  });
  app.get('/Vendor/VendorSchoolMapping', function(req, res) {
    res.render('pages/Vendor/VendorSchoolMapping.ejs');
  });
  app.get('/Vendor/VendorClassMapping', function(req, res) {
    res.render('pages/Vendor/VendorClassMapping.ejs');
  });
  app.get('/Agent/AgentReg', function(req, res) {
    res.render('pages/Agent/AgentReg.ejs');
  });
  app.get('/Admin/Report', function(req, res) {
    res.render('pages/Admin/Report.ejs');
  });

 };  

